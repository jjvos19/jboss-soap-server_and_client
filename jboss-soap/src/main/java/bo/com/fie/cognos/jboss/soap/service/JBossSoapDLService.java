package bo.com.fie.cognos.jboss.soap.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.jws.soap.SOAPMessageHandler;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

@WebService(endpointInterface = "bo.com.fie.cognos.jboss.soap.service.IJBossSoapService")
@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
public class JBossSoapDLService implements IJBossSoapService {
	@Resource
	WebServiceContext wsc;

	@WebMethod	
	public String saludar(String nombre) throws Exception {
		MessageContext messageContext = wsc.getMessageContext();
		Map<String, Object> headers = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
		List<String> usuario = (List<String>) headers.get("usuario");
		List<String> password = (List<String>) headers.get("password");
		String cadena;
		if (usuario != null && password != null && usuario.size() > 0 && password.size() > 0
				&& usuario.get(0).equals("jj") && password.get(0).equals("JVO")) {
			cadena = "Bienvenido JJ";
		} else {
			throw new Exception("Error en la autenticacion");
		}
		return cadena;
	}

	public String saludarSinWebMethod(String nombre) {
		return "Hola " + nombre;
	}

	private String saludarPrivado(String nombre) {
		return "Hola " + nombre;
	}
}
