package bo.com.fie.cognos.jboss.soap.validators;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class ValidationHandler implements SOAPHandler<SOAPMessageContext> {

	public boolean handleMessage(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("handleMessage");
		boolean bandera = validarDatos(context);
		return bandera;
	}

	public boolean handleFault(SOAPMessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("handleFault");
		return false;
	}

	public void close(MessageContext context) {
		// TODO Auto-generated method stub
		System.out.println("close-handle");
	}

	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		System.out.println("getHeader-handle");
		return null;
	}

	public boolean validarDatos(SOAPMessageContext context) {
		// Se verifica que el mensaje sea de ingreso (Request)
		boolean bandera = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (!bandera) {
			HttpServletRequest request = (HttpServletRequest) context.get(MessageContext.SERVLET_REQUEST);
			System.out.println("IP: " + request.getRemoteAddr());
			System.out.println("Host: " + request.getRemoteHost());
			System.out.println("Header-Usuario: " + request.getHeader("usuario"));
		}
		return !bandera;
	}

}
