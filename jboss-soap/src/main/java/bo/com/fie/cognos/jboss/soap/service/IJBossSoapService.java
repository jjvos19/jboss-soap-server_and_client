package bo.com.fie.cognos.jboss.soap.service;

import javax.jws.HandlerChain;
import javax.jws.WebService;

@WebService
@HandlerChain(file = "handler-chain.xml")
public interface IJBossSoapService {
	// Solo los metodos que estan en la interface se muestran en el servicio.
	String saludar(String nombre) throws Exception;

	String saludarSinWebMethod(String nombre);
}
